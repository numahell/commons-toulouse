# Retours communecter

## Expérience utilisateur solo

### UX

* l'image d'attente un peu trop lourde, et pas forcément utile pour certains cas (là où on ne change pas d'écran)

### Code

* framework php ou from scratch ? trouvé : Yii
* quelles sont les bibliothèques JS ? on dirait qu'il y en a beaucoup
* leaflet pour la carto : très bien

### Performances

* lourdeur à charger : mes orgas total 5,8Mo, 183 requêtes. Ma commune jusqu'à 10Mo ... C'est visiblement dû au chargement systématique de la carte.
* pourquoi la carte est-elle chargée en mode informations ?
* images de profil : retailler l'image (on a par exemple une image en 4K, y compris sur la vignette)
* on dirait que les images d'attente sont un peu lourde (145ko pour un spin c'est un peu trop)
* JQueryUI, à quoi ça sert ? Est-ce vraiment utile ? parce qu'il devient obsolète.
* minifier et concatener les js et css aidera à la performance (je peux aider)
* 600Ko de CSS, c'est bof, peut-être qu'i y a des doublons ? (je peux aider là-dessus)
* il y a du style inline (je peux aider)
* 2 version de JQuery ?
* faire des sprites pour les pictos peut aider aussi

### traduction

traduction gettext ?
