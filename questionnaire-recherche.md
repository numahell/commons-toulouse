Répondu le 3/02/16

Etude sur les "communs francophones"
Nous connaissons un phénomène de (re -) développement des "communs" (http://assemblee.encommuns.org/). Des mouvements se structurent. Il devient légitime d'étudier ces évolutions d'un point de vue scientifique.

Nous souhaitons en réaliser une première approche. Les problématiques sont donc basiques et visent à comprendre 1) les perceptions de la dynamique 2) les perceptions des organisations émergentes que représentent les "assemblées des communs" 3) les raisons plus structurelles du développement des "communs".

Les réponses à l'étude peuvent prendre jusqu'à une vingtaine de minutes. Le questionnaire peut être utilisé lors d'un atelier et servir de format d'animation (après une première présentation des communs et des assemblées).

Les données fournies seront évidemment anonymisées lors du traitement et vous avez le droit de demander une modification de vos réponses : mailto : rtrillard@gmail.com

## Comment percevez - vous le développement des "communs" ? *

J'ai découvert les Communs depuis 2013 lors du festival "Villes en biens communs".
J'ai l'impression que depuis les idées liées aux communs se répandent dans divers milieux : écologie, logiciel et culture libre, ESS, ... et le fait d'être débattu dans une loi montre que ça n'est pas simplement une lubie de quelques hippies (ou de gus dans un garage).

Les Communs commencent à être de mieux en mieux définis et définissables : une ressource partagée, gérée par un groupe ou une communauté, qui édicte les règles pour gérer et la protéger. Parler de propriété autre que privée ou publique est intéressant également.

Il y a encore du travail pour rendre visible concrètement les communs, mais les festivals de 2013 et 2015 ont bien aidé, et la suite arrive :)

## Comment est - ce que vous les expliqueriez à quelqu'un ?

## Comment percevez - vous de la dynamique fédérative qu'ils entraînent ? *

J'ai participé l'an dernier à deux mouvements : Alternatiba, et le Temps des communs.
Il y a dans les deux mouvement l'envie de fédérer sur ce qui rassemble les organisation participantes, l'intuition d'être plus complémentaires que concurrents, l'envie de montrer des solutions positives, de construire la société autrement que sur le mode consommation / production.

Dans le groupe dans lequel je suis, les domaines sont très divers : énergie renouvelable, logiciel libre, savoirs communs, tiers-lieux, "ville en transition", une maison de quartier, une régie de quartier, des artistes, des entreprises à orientation sociale et solidaires, des bibliothèques ou musées, des coopératives d'achat, ...

Mais il y a un revers au succès que connaissent les communs : le risque de récupération politique : on a vu apparaître sur les listes des termes comme "en commun", "commun", etc. avec plus ou moins de sincérité dans les intentions des candidats.

Un autre "risque", ou plutôt là où il faut faire attention, c'est que ce mouvement puisse toucher toutes les strates de la société, pas seulement les "intellectuels", et que chacun avec son contexte et son environnement se sente inclus dans ce mouvement. Les communs existent partout, la conscience que c'est un commun pas forcément.

## Quelle(s) raison(s) fondamentale(s) explique(nt) pour vous le (re-) développement des "communs" ? *

J'imagine que Elinor Ostrom y est pour quelque chose.
Il y a une prise de conscience depuis longtemps que la société dans laquelle nous sommes produit beaucoup d'inégalités sociales et de catastrophes écologiques : jusque là on était beaucoup dans la lutte (Greenpeace, ATTAC) ou le soin (les ONG et autres associations caritative).
Cette possibilité des Communs est peut-être l'idée qui manquait pour proposer de construire autre chose, et de se fédérer autour : une idée-catalyseur.


## Vous êtes ? *
une femme
un homme
## Quel âge avez - vous ? *
## Dans quel pays vous vous situez, quelle ville ? *
## Quel est votre parcours (formation) et quelles sont vos principales activités professionnelles et/ou associatives ? *
De formation scientifique : électronique puis informatique. Je suis développeuse dans une entreprise qui fait des services autour de logiciels libres. Je milite également dans une association qui promeut le logiciel libre.
Depuis cette année, je suis inscrite à distance en L1 économie et sociologie

## Souhaitez - vous être informés de l'avancée de la recherche ?
Oui
Non
## Souhaitez - vous recevoir les informations de l' "assemblée des communs" de Rennes ?
Oui
Non
## Adresse électronique
