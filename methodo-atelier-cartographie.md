% Atelier cartographier les communs
% Groupe Toulouse en biens communs
% 5 février 2015

---
lang: french
fontfamily: quattrocento
geometry: margin=1.25in
...

https://semestriel.framapad.org/p/ateliers-carto-communs

# Méthodologie

Il y aurait donc au moins deux aspects sur cet atelier :

* les données
* les outils pour saisir et valoriser les données

## Les données

On aura besoin de données de qualité (vérifiées, avec des motclés pertinents), récentes, et réutilisables (licence permettant la réutilisation)

### Atelier brassage de communs (common-storming)

nom :
important

Objectif
:     constituer le groupe, récolter de la matière, sortir une typologie des communs

Durée totale
:     2h en comptant l'apéro

#### Déroulement

donner les durées, commencer à l'heure : quelqu'un peut être gardien du temps

Moment d'activité - 3/4 d'heure à 1h

* étape 0 : tour de table bref (prénom + assez), présentation de la démarche, ajustements proposés
    niveau d'énergie à demander à l'ouverture
* étape 1 : lister en vrac les communs que nous connaissons sur des grands postits
* regroupement : chacun présente les communs qu'il a repérés
    rôle de scribe lors de la description orale d'un commun sur un pad par exemple => ensuite montrer, suscite la contribution future
* étape 2 : ajouter des mots-clés sur des petits postits
    Le scribe peut saisir sur le pad ou l'outil les mots-clés à la volée
* conclusion : impression, retour sur investissement du temp (même méthode )

Moment convivial ensuite

Pas grave si ce n'est pas exhaustif, une description incomplète pour susciter les contributions par la suite, notamment des porteurs des communs.

qd il y a un pb, mettre le pb sur la table et demander aux gens d'aider à résoudre

Prendre des photos à chaque étape de l'atelier.

Dans le même atelier si on est assez nombreux, on peut faire une équipe qui s'occupe de la saisie sur l'outil choisi (voir l'atelier Saisie).

#### Matériel

* en intérieur
* tables chaises
* connexion internet + 1 ou 2 PC
* postits petits et grands
* une affiche explicitant les composantes nécessaire d'un commun
* stylos, feutres, rouleau adhésif

Parmi le groupe, plusieurs rôle :

* un animateur
* un gardien du temps
* un scribe (peut tourner)

### Sur le terrain

Objectif
:    qualifier les communs

Rencontrer les acteurs (parmi lesquels des membres de temps des communs d'ailleurs), par petits groupes (pas obligé que tout le groupe se déplace, mais deux ou trois personnes)

* interviewer les commoners,
* documenter leurs démarches,
* illustrer avec des photos,
* raconter la sortie
* saisir les infos

Occasion de quelques sorties en groupe. Ça peut se faire aussi durant les common parties. On peut même en faire la promo (on a un blog toulouse en biens communs ?)

## Les outils

Objectifs
:    saisir et valoriser ces données. Doit être suffisamment ergonomique pour encourager la contribution

Pour le moment on va tester communecter.org.

### Saisie asynchrone

Chacun commence à saisir les communs dont ils ont connaissance.

### Atelier saisie

Objectif : saisir les données ensemble, faire des retours utilisateurs

Peut se faire en même temps que l'atelier brassage de communs pendant l'étape 2.

Ce serait intéressant qu'une personnes note les remarques des autres qui vont saisir l'information (ce rôle peut tourner). C'est un peu comme un test utilisateurs.

Un ou plusieurs groupes de 2 :

* un-e qui saisit les informations
* un-e autre qui note les remarques de celui ou celle qui saisit

### Data visualisation

Réfléchir à plusieurs façon de mettre en avant ces informations sur les communs (carte, fiche, ...). Un gros travail existe déjà sur communecter, probablement à améliorer.

Peut-être discuter de liens avec OSM, Wikipedia, etc. ?

### Atelier contribution (peut-être)

Objectif
:    contribuer à l'outil si on a accroché

Plutôt orienté développeurs, mais aussi traduction, documentation

## Chronologie

* dès maintenant : saisie de quelques communs sur l'outil
* février-mars : atelier common-storming + atelier saisie
* mars à juin : rencontres sur le terrain avec des commoners
* mai puis juillet : apéro des cartographes

safari de communs
( kokopelli en ariège, se sentent pas proches de la démarche car méfiant)
